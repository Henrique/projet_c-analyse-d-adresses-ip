/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet : 2                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé :  Analyse d’adresses IP                                          *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : Antunes Dos Santos Nabais Daniel                             *
*                                                                             *
*  Nom-prénom2 : Rodrigues Henrique                                           *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier : IP.c                                                      *
*                                                                             *
******************************************************************************/

#include "IP.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

bool verifFormat(char *ip){
	
	//Vérifie si la taille de l'Ip est correcte
	if(strlen(ip)!=41){
		return false;
	}
	
	//Vérifie si l'Ip est en binaire et si les '.' et le '/' est à la bonne position
	int c=0;
	for( int i=0;i<strlen(ip);i++){
		if(ip[i]== '0' || ip[i]=='1'){
				c++;
		}else if(ip[8]=='.' && ip[17]=='.' && ip[26]=='.' && ip[35]=='/'){
				c++;
		}else{
			return false;
		}
	}
	return true;
}

void extraireChamps(char *ip,char *ipChamp1,char *ipChamp2,char *ipChamp3,char *ipChamp4,char *Masque){
	int j=0;
	int k=0;
	int l=0;
	int m=0;
	int c=0;
	//Permet d'extraire les champs de l'Ip dans les 4 ip Champs et le Champ pour le masque
	for(int i=0;i<strlen(ip);i++){
		if(i<8){
			ipChamp1[i]=ip[i];
		}
		if(i>8 && i<17){
			ipChamp2[j]=ip[i];
			j++;
		}
		if(i>17 && i<26){
			 ipChamp3[k]=ip[i];
			k++;
		}else if(i>26 && i<35){
			ipChamp4[l]=ip[i];
			l++;
		}else if(i>35){
			Masque[m]=ip[i];
			m++;
		}else{
			c++;
		}
	}

}

int convertValNumip ( char* ipChamp1 ){
	int e = 7 ;
	int xyz1 = 0;
	
	//extraire les ip champ binaire pour les avoir en decimal
	for( int i=0 ; i<8 ; i++) {
		 xyz1 = xyz1 + ((int) ipChamp1[i]- 48) * pow( 2 , e) ;
		 e = e-1;
	}
	e = 4 ;
	
	return xyz1;
}
int convertValNummasque ( char* Masque ){
	int e = 4 ;
	int xyzMasque = 0;
	
	
	
	//extraire le masque binaire pour l avoir en decimal
	for( int i=0 ; i<5 ; i++) {
		xyzMasque = xyzMasque + ((int) Masque[i]- 48) * pow( 2 , e) ;
		
		e = e-1;
	}
	return xyzMasque;
}

void decodeIP ( int valeurip1 ,  int valeurip2 ,  int valeurip3 ,  int valeurip4 ) {
	//Vérifie si c'est une Ip de classe A
	//Si elle est privée ou public
	//Si c'est une adresse localhost ou de broadcast
	if( valeurip1 < 128 ){
		printf ("Ip de classe A\n ");
		if ( valeurip1 == 10  ){
			printf ("Ip a usage privé\n");
		} else {
			printf ("Ip a usage publique\n");
		}
		
		if (  valeurip1 =127 ){
		printf (" c'est une adresse de localhost\n");
		
		}
		
		if (  valeurip4 =255 ){
		printf (" c'est une adresse de broadcast\n" );
		}
	}
	//Vérifie si c'est une Ip de classe B
	//Si elle est privée ou public
	//Si c'est une adresse de broadcast
	if ( valeurip1 >= 128  && valeurip1 <= 191){
		printf ("Ip de classe B\n") ;
		if ( valeurip1 == 176 && valeurip2 >= 16 && valeurip2 <= 31  ){
			printf ("Ip a usage privé\n");
		} else {
			printf ("Ip a usage publique\n");
		}
		if (  valeurip4 == 255 ){
		printf (" c'est une adresse de broadcast\n" );
		}
	}
	//Vérifie si c'est une Ip de classe C
	//Si elle est privée ou public
	//Si c'est une adresse broadcast
	if ( valeurip1 >= 192  && valeurip1 <= 223 ){
		printf ("Ip de classe C ") ;
		if ( valeurip1 == 192 && valeurip2 == 168  ){
			printf ("Ip a usage privé\n");
		} else {
			printf ("Ip a usage publique\n");
		}
		
		if (  valeurip4 == 255 ){
		printf (" c'est une adresse de broadcast\n" );
		}
		
	}
	//Vérifie si c'est une Ip de classe D
	//Si c'est une adresse de multicast
	if  ( valeurip1 >= 224  && valeurip1 <= 239 ){
		printf ("Ip de classe D\n") ;
		printf ("c'est une adresse de multicast\n");
	}
	//Vérifie si c'est une Ip de classe E
	if  ( valeurip1 >= 240  ){
		printf ("Ip de classe E\n") ;
		
	}
}
//Permet d'afficher correctement les informations de l'application
void affichage ( int valeurip1 ,  int valeurip2 ,  int valeurip3 ,  int valeurip4 , int valeurmasque ){
	decodeIP( valeurip1 ,   valeurip2 ,  valeurip3 ,   valeurip4 );
	printf ( "l'ip est : %d.%d.%d.%d/%d\n" , valeurip1 , valeurip2 , valeurip3, valeurip4 , valeurmasque );
  

}
