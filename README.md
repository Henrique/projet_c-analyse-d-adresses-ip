***Application d'extraction de certaines informations d'une adesse Ip et de son masque Réseau ***

Binome :                                         
Antunes Dos Santos Daniel

Rodrigues Henrique                                           


***********************************************************************************************************************

Vérification du format : 

//Permet de vérifier si le format de l'Ip est correcte pour pouvoir la convertir et la décoder
Nom de la fonction :  verifFormat

Paramètres en entrée : ip à vérifier
En sortie : Renvoie vrai si l'ip à la bonne taille (41 caractères) et si elle est binaire.L'ip doit avoir le format
nécessaire de type xxxxxxxx.xxxxxxxx.xxxxxxxx.xxxxxxxx/xxxxx.Sinon elle renvoie faux.

***********************************************************************************************************************

Extraction des champs :

// Permet d'extraire les différents champs de l'IP pour pouvoir les convertir plus facilement
Nom de la fonction : extraireChamps

Paramètres en entrée : ip vérifiée précédement ainsi que les 5 champs en entrée (4 champs de l'ip + champ du masque)
En sortie : Renvoie les 5 champs de l'Ip et le champ du masque séparément

***********************************************************************************************************************

Convertion en valeurs numériques :	(2 fonctions)

//Permet de convertie les 4 champs de l'Ip de char en int
Nom de la première fonction : convertValNumip

Paramètres en entrée : Prends l'un des 4 champs (char) de l'Ip dans l'ordre
En sortie : Renvoie le champ converti en int (valeur décimale)




//Permet de convertie le champs du masque de char en int
Nom de la deuxième fonction : convertValNummasque

Paramètres en entrée :Prends le masque(char)
En sortie : Renvoie le masque converti en int

***********************************************************************************************************************

Décodage de l'IP :
//Permet d'afficher la Classe de l'Ip et de savoir si l'ip est privée ou public et si c'est une Ip de multicast,
localhost ou broadcast
Nom de la fonction :décodageIP

Paramètres en entrée : Les 4 valeurs Ip ( champs convertis en decimal)
En sortie : On obtient les informations sur l'IP : Classe / public ou privé ainsi que localhost,broadcast ou multicast

***********************************************************************************************************************

Affichage de l'application :
Nom de la fonction : affichage

Paramètres en entrée : Les 4 valeurs de l'Ip
En sortie :Un affichage du message final avec les informations récupérés précédement
***********************************************************************************************************************

